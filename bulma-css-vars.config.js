const { hsl } = require('bulma-css-vars')

const appColors = {
  primary: '#5b7424',
  primaryLight: '#5b7424',
  primaryDark: '#5b7424',
  pprimaryInvert: '#5b7424',
}

appColors['scheme-main'] = appColors['primary'];
appColors['text'] = appColors['primary'];
appColors['text-strong'] = appColors['primary'];

module.exports = {
  sassEntryFile: 'resources/scss/main.scss',
  jsOutputFile: 'resources/scss/bulma-generated/bulma-colors.js',
  sassOutputFile: 'resources/scss/bulma-generated/generated-vars.sass',
  cssFallbackOutputFile: 'resources/scss/bulma-generated/generated-fallbacks.css',
  colorDefs: appColors,
  globalWebVar: true,
  transition: '0.5s ease',
}
