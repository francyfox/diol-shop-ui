# Diol test shop
![version](https://img.shields.io/badge/Laravel-9.15.0-green)
![version](https://img.shields.io/badge/php-8.1-orange)
![version](https://img.shields.io/badge/npm-8.9.0-red)
![version](https://img.shields.io/badge/msql-8.0-blue)
![version](https://img.shields.io/badge/status-development-blue)

Это чистый laravel

[Miro dashboard](https://miro.com/app/board/uXjVO9QcXe4=/?share_link_id=424385726168) (там же теперь есть мудборд)

## TODO
Необходимо сделать чистый UI (с тупыми компонентами) для
- [x] админка авторизация
- [ ] админка главная (дашборд)
- [ ] админка список
- [ ] админка настройки (Общие, Магазин, Пользователи)

- [x] главной магазина
- [x] каталог
- [x] продукт
- [ ] корзина
- [ ] оформление
- [ ] Модалки

Далее идет:

1. Создание базового css фреймворка. Функция перевода цветов в hsla, прозрачности, насыщенности, яркости.  
Цвета, типографика, отступы, миксины медиазапросов, величины контейнеров,
2. Проработка базовых компонентов. Вынесение props компонентов, оформление их в storybook.
3. Создание базового UI kit-а дизайнера.
4. Делаем компоненты умными.

Это тока по фронтенд часть.

### Ресурсы:

1. [Готовый тестовый модуль магазина на Buefy](https://github.com/14nrv/buefy-shop)
2. [Документация](https://buefy.org/documentation/)
3. [Пошаговая создание CMS на Laravel + Vue с репой](https://github.com/neoighodaro/laravel-vue-cms?ref=morioh.com&utm_source=morioh.com)
4. [Неполный figma UI kit от community Bulma](https://www.figma.com/community/file/1046059834905417658)

## Docker configuration for old diol shop
Полная сборка и обычный быстрый запуск

    docker-compose up --build
    docker-compose up -d --remove-orphans 

Разрешить запись логов

    sudo chmod -R o+w storage 
    sudo chmod -R o+w bootstrap

Создайте папку кэша с правами записи

    mkdir -m755 bootstrap/cache
    mkdir -m755 storage/framework/views

Апдейт композера

    docker exec -it php /bin/bash
    composer selfupdate --1  (laravel < 7.0)
    composer install
Импорт базы (используйте дамп i-keys) пароль по умолчанию 123456

    docker exec -it db /bin/bash
    mysql -u root -p shop < db_name.sql
