<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::resource('projects', 'ProjectsController');

Route::prefix('admin')->group(function () {

    Route::get( ('/user'), function () {
        return view('common.security.user.page');
    })->name('user');

    Route::get(('/user/{id}/dashboard'), function ($id) {
        return view('common.security.user.dashboard.page');
    })->name('dashboard');

    Route::get(('/user/{id}/profile'), function ($id) {
        return view('admin.routers.page');
    })->name('profile');

    Route::get(('/user/{id}/router'), function ($id) {
        return view('admin.routers.page');
    })->name('router');

    Route::get(('/user/{id}/forms'), function ($id) {
        return view('admin.routers.page');
    })->name('forms');

    Route::get(('/user/{id}/entity'), function ($id) {
        return view('admin.entity.page');
    })->name('entity');
});

Route::get( ('/'), function () {
    return view('client.page.shop.page');
})->name('main');

Route::prefix('shop')->group(function () {
    Route::get( ('/list'), function () {
        return view('client.page.shop.list.page');
    })->name('list');

    Route::get( ('/list/product/{id}'), function ($id) {
        return view('client.page.shop.list.product.page');
    })->name('product');
});
