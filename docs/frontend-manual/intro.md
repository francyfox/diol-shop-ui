# Мануал для верстки v0.3

1. [Общее положение](https://gitlab.com/francyfox/diol-shop-ui/-/blob/main/docs/frontend-manual/page/_about.md)
2. [Структура проектов](https://gitlab.com/francyfox/diol-shop-ui/-/blob/main/docs/frontend-manual/page/_structure.md)
3. [Lint Rules](https://gitlab.com/francyfox/diol-shop-ui/-/blob/main/docs/frontend-manual/page/_lint_rules.md)
5. [Чек-лист Фронтенд](https://gitlab.com/francyfox/diol-shop-ui/-/blob/main/docs/frontend-manual/page/_check_frontend.md)
6. [Требования от отдела по оптимизации (есть по фронтенду, но в основном бэк)](https://gitlab.com/francyfox/diol-shop-ui/-/blob/main/docs/frontend-manual/page/_check_backend.md)
7. [Тестирование фронтенда - пишется](https://gitlab.com/francyfox/diol-shop-ui/-/blob/main/docs/frontend-manual/page/_test_frontend.md)



## Дополнительно
1. [Подключение к VPN, SSH](https://gitlab.com/francyfox/diol-shop-ui/-/blob/main/docs/frontend-manual/page/_connect.md)