

# Подключение к ssh, openVPN, Mercurial hg / GiTLab, RDP
Хочу упомянуть, большую часть windows софта можно устанавливать через https://chocolatey.org/
Если вам нравиться концепция репозитарий linux, вы оцените.
## openVPN
##### Необходимо:
" * - любое название"
- client
-- *.p12
-- *.conf (linux) || *.ovpn (windows) // файл конфигурации
-- pass.conf (linux)

Подключение происходит посредством клиента OpenVpn.
Ссылки на скачивание
Windows: https://openvpn.net/vpn-client/

(Вариант с linux сложнее, в сети есть массу других различных вариантов, существуют различные нюансы при настройке окружения на разных оболочках или типов OS, но несомненно вы столкнетесь с трудностями и потеряете время, удачи)
На linux он может иметь различные GUI оболочки, в зависимости от выбранной графики . Для арча использую отдельный клиент, но можно использовать и нативный встроенный в систему
Ubuntu network-manager-openvpn
Gnome network-manager-openvpn-gnome

Arch AUR https://archlinux.org/packages/community/any/qopenvpn/ (GUI клиент)

~~Нативный вариант выглядит так:
Сетевое соединение -> Нажимаем добавить (+) -> OpenVpn
Шлюз берем и конфига openvpn 109.195.189.149 (убедитесь что стоит стандартный порт шлюза 1194 в дополнительных параметрах)
*... Продолжение следует, а может быть и нет*~~

Вариант с qOpenVPN (он же classic openvpn) (по факту можно запускать и через консоль, gui добавляет лишь ярлычок в трее и выбор конфига)
Распаковываем в /etc/openvpn/client два файла. Переименовываем расширение *.ovpn в *.conf и добавляем строчку askpass pass.conf

    #OpenVPN Client conf
    tls-client
    client
    nobind
    dev tun
    proto tcp
    tun-mtu 1400
    remote 109.195.189.149 1194
    pkcs12 DanilGolota.p12
    cipher AES-256-CBC
    auth SHA512
    verb 3
    remote-cert-tls server
    askpass pass.conf
    verify-x509-name 109x195x189x149.static-business.tula.ertelecom.ru name

Внутри файла pass.conf указываем пароль от openvpn
Нажимаем на иконку в трее и делаем Start. Как выполнить без gui можете посмотреть в интернете. Таким образом не придется мучатся с генерированием отдельных сертификатов.

## SSH
Для подключение на windows просто используйте pageant.exe и импортируйте id_rsa.
id_rsa это приватный ключ, для доступа на сервер нужно сгенирировать через puttygen.exe
Публичный ключ в формате .pub и отправить администратору.

Используйте putty или windows powershell ssh
ваш_логин@192.168.1.99

Для подключение на linux вам нужно преобразовать id_rsa в стандартный RSA private key.

![enter image description here](https://i2.wp.com/www.nextofwindows.com/wp-content/uploads/2015/03/2015-03-25_1259_001.png)

id rsa должен иметь Pem формат и начинаться на

    -----BEGIN RSA PRIVATE KEY-----
Для этого экспортируйте через вкладку conversions  **Export ssh.com key**
https://www.ssh.com/academy/ssh/putty/linux/puttygen  -- описаны команды для конвертации, нужно возможно скачать putty для linux (gui в linux нету, по крайней мере я не знаю) , нас интересует **private-sshcom**
Запустите ssh-agent

    _eval_ $(_ssh_-_agent_ -s)
Установите права 600 id_rsa и импортируйте приватный ключ (id rsa должен быть в папке .ssh, а она в папке пользователя **~/.ssh**)

    sudo chmod 600 id_rsa && ssh-add id_rsa
Если не было ошибок подключаемся, а если были читаем ошибку и включаем голову. (Используй силу Люк)

## Mercurial
В кратце про ртуть. Установите hg и клиент Tortoise HG.
Ссылка на скачивание
https://tortoisehg.bitbucket.io/

в конфигурации ртути ~/.hgrc (каталог home) задайте имя пользователя на латинице.
```
[ui]
username = Gena Bukin
``````
В случае отказа доступа к серверу (ошибка piblic key), добавьте в блок [ui] путь к id_rsa (pem)
```
[ui]
ssh = ssh -C -i ~/.ssh/id_rsa-mercurialkey
```
