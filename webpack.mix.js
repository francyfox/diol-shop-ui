const mix = require('laravel-mix');
require('laravel-mix-blade-reload');
require('laravel-mix-purgecss');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

let productionSourceMaps = false;

mix.js('resources/js/app.js', 'public/js')
    .vue()
    .sass('resources/scss/custom-bulma.scss', 'public/css')
    .sourceMaps(productionSourceMaps, 'source-map')
    .postCss('resources/postCss/app.css', 'public/css',[
      require('postcss-nested'),
      require('postcss-simple-vars'),
      require('autoprefixer')
    ])
    .sourceMaps(productionSourceMaps, 'source-map')
    .bladeReload();

