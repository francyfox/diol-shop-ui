import Listfilter from './ListFilter.vue';

export default {
    title: 'Listfilter',
    component: Listfilter,
};

export const Default = (args, {argTypes}) => ({
    props: Object.keys(argTypes),
    components: { Listfilter },
    template: `<ListFilter v-bind="$props" />`,
});
