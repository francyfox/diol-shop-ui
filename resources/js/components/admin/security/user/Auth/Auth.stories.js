import Auth from './Auth.vue';

export default {
    title: 'Auth',
    component: Auth,
};

export const Default = (args, {argTypes}) => ({
    props: Object.keys(argTypes),
    components: { Auth },
    template: `<Auth v-bind="$props" />`,
});
