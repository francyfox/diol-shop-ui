import Register from './Register.vue';

export default {
    title: 'Register',
    component: Register,
};

export const Default = (args, {argTypes}) => ({
    props: Object.keys(argTypes),
    components: { Register },
    template: `<Register v-bind="$props" />`,
});
