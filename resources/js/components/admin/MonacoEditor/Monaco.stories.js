import Monoco from './Monoco.vue';

export default {
    title: 'Monoco',
    component: Monoco,
};

export const Default = (args, {argTypes}) => ({
    props: Object.keys(argTypes),
    components: { Monoco },
    template: `<Monoco v-bind="$props" />`,
});
