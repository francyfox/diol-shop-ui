import Breadcrumbs from './BreadCrumbs.vue';

export default {
    title: 'Breadcrumbs',
    component: Breadcrumbs,
};

export const Default = (args, {argTypes}) => ({
    props: Object.keys(argTypes),
    components: { Breadcrumbs },
    template: `<BreadCrumbs v-bind="$props" />`,
});
