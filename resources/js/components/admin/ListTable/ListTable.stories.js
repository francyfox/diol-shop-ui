import Listtable from './ListTable.vue';

export default {
    title: 'Listtable',
    component: Listtable,
};

export const Default = (args, {argTypes}) => ({
    props: Object.keys(argTypes),
    components: { Listtable },
    template: `<ListTable v-bind="$props" />`,
});
