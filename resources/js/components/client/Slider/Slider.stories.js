import Slider from './Slider.vue';

export default {
    title: 'Slider',
    component: Slider,
};

export const Default = (args, {argTypes}) => ({
    props: Object.keys(argTypes),
    components: { Slider },
    template: `<Slider v-bind="$props" />`,
});
