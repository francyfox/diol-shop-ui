import Productactions from './ProductActions.vue';

export default {
    title: 'Productactions',
    component: Productactions,
};

export const Default = (args, {argTypes}) => ({
    props: Object.keys(argTypes),
    components: { Productactions },
    template: `<ProductActions v-bind="$props" />`,
});
