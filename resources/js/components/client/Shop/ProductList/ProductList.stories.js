import Productlist from './ProductList.vue';

export default {
    title: 'Productlist',
    component: Productlist,
};

export const Default = (args, {argTypes}) => ({
    props: Object.keys(argTypes),
    components: { Productlist },
    template: `<ProductList v-bind="$props" />`,
});
