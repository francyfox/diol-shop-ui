import Popular from './Popular.vue';

export default {
    title: 'Popular',
    component: Popular,
};

export const Default = (args, {argTypes}) => ({
    props: Object.keys(argTypes),
    components: { Popular },
    template: `<Popular v-bind="$props" />`,
});
