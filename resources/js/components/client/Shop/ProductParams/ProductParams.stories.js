import Productparams from './ProductParams.vue';

export default {
    title: 'Productparams',
    component: Productparams,
};

export const Default = (args, {argTypes}) => ({
    props: Object.keys(argTypes),
    components: { Productparams },
    template: `<ProductParams v-bind="$props" />`,
});
