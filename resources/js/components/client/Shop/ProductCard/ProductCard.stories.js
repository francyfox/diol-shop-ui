import Productcard from './ProductCard.vue';

export default {
    title: 'Productcard',
    component: Productcard,
};

export const Default = (args, {argTypes}) => ({
    props: Object.keys(argTypes),
    components: { Productcard },
    template: `<ProductCard v-bind="$props" />`,
});
