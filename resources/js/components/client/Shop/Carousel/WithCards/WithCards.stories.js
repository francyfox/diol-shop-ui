import Withcards from './WithCards.vue';

export default {
    title: 'Withcards',
    component: Withcards,
};

export const Default = (args, {argTypes}) => ({
    props: Object.keys(argTypes),
    components: { Withcards },
    template: `<WithCards v-bind="$props" />`,
});
