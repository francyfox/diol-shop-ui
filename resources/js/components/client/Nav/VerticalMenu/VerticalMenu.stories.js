import Verticalmenu from './VerticalMenu.vue';

export default {
    title: 'Verticalmenu',
    component: Verticalmenu,
};

export const Default = (args, {argTypes}) => ({
    props: Object.keys(argTypes),
    components: { Verticalmenu },
    template: `<VerticalMenu v-bind="$props" />`,
});
