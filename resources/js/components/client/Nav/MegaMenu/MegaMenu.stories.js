import Megamenu from './MegaMenu.vue';

export default {
    title: 'Megamenu',
    component: Megamenu,
};

export const Default = (args, {argTypes}) => ({
    props: Object.keys(argTypes),
    components: { Megamenu },
    template: `<MegaMenu v-bind="$props" />`,
});
