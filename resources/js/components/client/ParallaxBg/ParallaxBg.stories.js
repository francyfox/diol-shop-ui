import Parallaxbg from './ParallaxBg.vue';

export default {
    title: 'Parallaxbg',
    component: Parallaxbg,
};

export const Default = (args, {argTypes}) => ({
    props: Object.keys(argTypes),
    components: { Parallaxbg },
    template: `<ParallaxBg v-bind="$props" />`,
});
