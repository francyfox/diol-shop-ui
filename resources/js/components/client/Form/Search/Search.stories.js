import Search from './Search.vue';

export default {
    title: 'Search',
    component: Search,
};

export const Default = (args, {argTypes}) => ({
    props: Object.keys(argTypes),
    components: { Search },
    template: `<Search v-bind="$props" />`,
});
