import Review from './Review.vue';

export default {
    title: 'Review',
    component: Review,
};

export const Default = (args, {argTypes}) => ({
    props: Object.keys(argTypes),
    components: { Review },
    template: `<Review v-bind="$props" />`,
});
