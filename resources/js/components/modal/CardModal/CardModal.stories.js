import Cardmodal from './CardModal.vue';

export default {
    title: 'Cardmodal',
    component: Cardmodal,
};

export const Default = (args, {argTypes}) => ({
    props: Object.keys(argTypes),
    components: { Cardmodal },
    template: `<CardModal v-bind="$props" />`,
});
