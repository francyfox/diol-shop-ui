import Buefy from 'buefy'
import Vuex from 'vuex'

// plugins
import VueMask from 'v-mask'
import vueFilePond from "vue-filepond";
import "filepond/dist/filepond.min.css";
import "filepond-plugin-image-preview/dist/filepond-plugin-image-preview.min.css";
import FilePondPluginFileValidateType from "filepond-plugin-file-validate-type";
import FilePondPluginImagePreview from "filepond-plugin-image-preview";

// fonts
import { library } from '@fortawesome/fontawesome-svg-core';
import { fas } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/vue-fontawesome";

// CSS VARS
// import { ColorUpdater } from 'bulma-css-vars';
// import { bulmaCssVariablesDefs } from './bulma-colors';

// const colorUpdater = new ColorUpdater(bulmaCssVariablesDefs);

/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

const FilePond = vueFilePond(
  FilePondPluginFileValidateType,
  FilePondPluginImagePreview
);

require('./bootstrap');

window.Vue = require('vue').default;

library.add(fas);
window.Vue.component('vue-fontawesome', FontAwesomeIcon);
window.Vue.component('file-pond', FilePond);
window.Vue.use(Buefy);
window.Vue.use(Vuex);
window.Vue.use(VueMask);

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

const files = require.context('./', true, /\.vue$/i)
files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
});
