/** @var { ProductState } */
const STATE = {
    view: {
      grid: Boolean,
      list: Boolean
    },
};

export default STATE;
