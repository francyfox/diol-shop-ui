import { SET_VIEW } from './mutation-types';

export default {
    /**
    *
    * @param { ProductState } state
    * @param { string } view
    */
    [SET_VIEW](state, { view }) {
        state.view = view;
    },
};
