import { SET_VIEW } from './mutation-types';

/**
*
* @param { function } commit
* @param { string } view
*/
export function setView({ commit }, { view }) {
    commit(SET_VIEW, { view });
}
