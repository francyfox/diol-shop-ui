Object.defineProperty(window, 'bulmaCssVarsDef',
  { enumerable: true, value: {"primary":{"calls":[{"fn":"color-invert","fnArg":null,"composeArg":null},{"fn":"dark-color","fnArg":null,"composeArg":null},{"fn":"light-color","fnArg":null,"composeArg":null}]},"scheme-main":{"calls":[]},"text":{"calls":[{"fn":"color-invert","fnArg":null,"composeArg":null}]},"text-strong":{"calls":[]}} }
)
