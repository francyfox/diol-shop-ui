@include('_head')
<section class="mb-5 has-background-primary">
    <div class="container">
        <div class="hero">
            <div class="hero-body pl-0">
                <p class="title has-text-white">
                    Регистрация и авторизация
                </p>
                <p class="subtitle has-text-white">
                    Заполните данные
                </p>
            </div>
        </div>
    </div>
</section>
<section class="section-auth mb-5">
    <div class="container">
        <user-form>
    </div>
</section>
@include('_bottom')
