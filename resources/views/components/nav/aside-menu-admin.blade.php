<aside class="aside is-placed-left is-expanded">
    <div class="aside-tools">
        <div class="aside-tools-label">
            <a class="has-text-white has-icon" href="/">
                <span class="icon"><i class="mdi mdi-arrow-left-drop-circle-outline"></i></span>
                <span class="menu-item-label">
                            Перейти на сайт
                        </span>
            </a>
        </div>
    </div>
    <div class="menu is-menu-main">
        <p class="menu-label">Общее</p>
        <ul class="menu-list">
            <li>
                <a href="/" class="is-active router-link-active has-icon">
                    <span class="icon"><i class="mdi mdi-desktop-mac"></i></span>
                    <span class="menu-item-label">Панель</span>
                </a>
            </li>
            <li>
                <a href="/profile" class="has-icon">
                    <span class="icon"><i class="mdi mdi-account-circle"></i></span>
                    <span class="menu-item-label">Профиль</span>
                </a>
            </li>
            <li>
                <a href="/profile" class="has-icon">
                    <span class="icon"><i class="mdi mdi-format-color-fill"></i></span>
                    <span class="menu-item-label">Внешний вид</span>
                </a>
            </li>
        </ul>
        <p class="menu-label">Модули</p>
        <ul class="menu-list">
            <li>
                <a href="./shop" class="has-icon">
                    <span class="icon "><i class="mdi mdi-cart"></i></span>
                    <span class="menu-item-label">Магазин</span>
                </a>
            </li>
            <li>
                <a href="./shop" class="has-icon">
                    <span class="icon "><i class="mdi mdi-newspaper-variant"></i></span>
                    <span class="menu-item-label">Статьи</span>
                </a>
            </li>
        </ul>
        <p class="menu-label">Контент-менеджер</p>
        <ul class="menu-list">
            <li>
                <a href="./router" class="has-icon">
                    <span class="icon "><i class="mdi mdi-square-edit-outline"></i></span>
                    <span class="menu-item-label">Страницы</span>
                </a>
            </li>
            <li>
                <a href="/form" class="has-icon">
                    <span class="icon"><i class="mdi mdi-table"></i></span>
                    <span class="menu-item-label">Формы</span>
                </a>
            </li>
            <li>
                <a href="/media" class="has-icon">
                    <span class="icon"><i class="mdi mdi-folder-multiple-image"></i></span>
                    <span class="menu-item-label">Медиа хранилище</span>
                </a>
            </li>
            <li>
                <a href="/l10n" class="has-icon">
                    <span class="icon"><i class="mdi mdi-translate"></i></span>
                    <span class="menu-item-label">Локализация</span>
                </a>
            </li>
        </ul>
        <p class="menu-label">SEO</p>
        <ul class="menu-list">
            <li>
                <a href="/target" class="has-icon">
                    <span class="icon"><i class="mdi mdi-bullseye-arrow"></i></span>
                    <span class="menu-item-label">Цели</span>
                </a>
            </li>
            <li>
                <a href="/target" class="has-icon">
                    <span class="icon"><i class="mdi mdi-file-percent"></i></span>
                    <span class="menu-item-label">Статистика</span>
                </a>
            </li>
        </ul>
        <p class="menu-label">Администратор</p>
        <ul class="menu-list">
            <li>
                <a href="/admin/user/1/entity" class="has-icon">
                    <span class="icon "><i class="mdi mdi-archive-outline"></i></span>
                    <span class="menu-item-label">Сущности</span>
                </a>
            </li>

            <li>
                <a href="tables.html" class="has-icon">
                    <span class="icon"><i class="mdi mdi-account-multiple"></i></span>
                    <span class="menu-item-label">Пользователи</span>
                </a>
            </li>


            <li>
                <a href="#" class="has-icon has-dropdown-icon">
                    <span class="icon "><i class="mdi mdi-archive-outline"></i></span>
                    <span class="menu-item-label">Логи</span>
                    <span class="dropdown-icon"><span class="icon"><i class="mdi mdi-plus"></i></span></span>
                </a>
                <ul>
                    <li>
                        <a href="tables.html">
                            <span class="menu-item-label">Миграции</span>
                        </a>
                    </li>

                    <li>
                        <a href="tables.html">
                            <span class="menu-item-label">История</span>
                        </a>
                    </li>
                </ul>
            </li>

            <li>
                <a href="tables.html" class="has-icon">
                    <span class="icon "><i class="mdi mdi-apple-icloud"></i></span>
                    <span class="menu-item-label">Бэкапы</span>
                </a>
            </li>

        </ul>
        <p class="menu-label">---------------------------</p>
        <ul class="menu-list">
            <li>
                <a href="/settings" target="_blank" class="has-icon">
                    <span class="icon"><i class="mdi mdi-cog"></i></span>
                    <span class="menu-item-label">Настройки</span>
                </a>
            </li>
        </ul>
    </div>
</aside>
