<nav class="navbar is-fixed-top" role="navigation" aria-label="main navigation">
    <div class="container is-fullhd">
        <div class="navbar-brand">
            <a class="navbar-item" href="https://buefy.org/documentation">
                <svg xmlns="http://www.w3.org/2000/svg" width="60" height="34" viewBox="0 10 85.04 50.04" xml:space="preserve"><g fill="#808283"><path d="M83.478 29.481c0-.466-.195-.884-.507-1.182a1.631 1.631 0 0 0-1.182-.507c-.133 0-.26.02-.383.05h-8.804c-.691 0-1.279.428-1.521 1.033a1.573 1.573 0 0 0-.149.948c.576 3.739.754 11.772-2.376 12.739a1.589 1.589 0 1 0 .941 3.037c5.386-1.666 5.172-10.521 4.793-14.479h5.86v12.314a1.638 1.638 0 1 0 3.278 0v-13.57c.03-.125.05-.251.05-.383zM58.201 27.941c-4.983 0-9.038 4.054-9.038 9.037s4.055 9.038 9.038 9.038c4.984 0 9.037-4.055 9.037-9.038s-4.052-9.037-9.037-9.037zm0 14.898a5.866 5.866 0 0 1-5.859-5.86 5.866 5.866 0 0 1 5.859-5.859 5.866 5.866 0 0 1 5.859 5.859 5.865 5.865 0 0 1-5.859 5.86z"/></g><g fill="#AE294D"><path d="M28.653 49.642c.905 0 1.639-.732 1.639-1.638V44.03c0-.906-.734-1.638-1.639-1.638-.133 0-.259.02-.383.049h-.461V29.864c.03-.125.049-.251.049-.383 0-.466-.196-.884-.507-1.182a1.627 1.627 0 0 0-1.181-.507c-.132 0-.259.02-.383.05h-8.804c-.69 0-1.279.428-1.52 1.033a1.588 1.588 0 0 0-.15.948c.555 3.604.731 11.185-2.062 12.589a1.63 1.63 0 0 0-1.418 1.476l-.007.067c0 .026-.007.05-.007.075v3.974c0 .905.733 1.638 1.639 1.638s1.639-.732 1.639-1.638v-2.285h11.918v2.285a1.637 1.637 0 0 0 1.638 1.638zM18.671 31.12h5.86v11.321H17.38c1.739-3.424 1.555-8.562 1.291-11.321zM45.339 28.089c-.389 0-.74.143-1.021.373a1.609 1.609 0 0 0-.606.501L35.159 39.84V29.757c0-.92-.734-1.667-1.639-1.667s-1.639.748-1.639 1.667v14.244c0 .301.084.578.22.822.096.341.297.656.607.882.732.532 1.756.37 2.289-.362L43.7 34.275v9.726c0 .922.733 1.668 1.639 1.668.904 0 1.639-.746 1.639-1.668V29.757c0-.92-.735-1.668-1.639-1.668z"/><path d="M36.022 50.949c-3.545 3.576-8.296 5.546-13.377 5.546-10.295 0-18.672-8.228-18.672-18.34 0-10.114 8.377-18.341 18.672-18.341h3.466v2.05a1.194 1.194 0 0 0 1.679 1.088l8.85-3.97a1.191 1.191 0 0 0 0-2.174l-8.85-3.971a1.189 1.189 0 0 0-1.679 1.087v1.918h-3.466C10.159 15.84 0 25.85 0 38.155c0 12.303 10.159 22.313 22.645 22.313 6.149 0 11.902-2.388 16.198-6.722a1.986 1.986 0 1 0-2.821-2.797z"/></g></svg>
            </a>

            <a role="button" class="navbar-burger" aria-label="menu" aria-expanded="false" data-target="navbarBasicExample">
                <span aria-hidden="true"></span>
                <span aria-hidden="true"></span>
                <span aria-hidden="true"></span>
            </a>
        </div>

        <div id="navbarBasicExample" class="navbar-menu">
            <div class="navbar-start">
                <div class="navbar-item">
                    <mega-menu/>
                </div>
                <a href="/" class="navbar-item">
                    Home
                </a>
                <a href="/shop/list" class="navbar-item">
                    Catalog
                </a>


                {{--            <div class="navbar-item has-dropdown is-hoverable">--}}
                {{--                <a class="navbar-link">--}}
                {{--                    Security--}}
                {{--                </a>--}}

                {{--                <div class="navbar-dropdown">--}}
                {{--                    <a href="/admin/user" class="navbar-item">--}}
                {{--                        Auth--}}
                {{--                    </a>--}}
                {{--                    <a href="/admin/user" class="navbar-item">--}}
                {{--                        Register--}}
                {{--                    </a>--}}

                {{--                    <hr class="navbar-divider">--}}
                {{--                    <a class="navbar-item">--}}
                {{--                        Forgot Password--}}
                {{--                    </a>--}}
                {{--                </div>--}}
                {{--            </div>--}}
            </div>

            @if(Route::currentRouteName() !== 'user')
                <div class="navbar-end">
                    <div class="navbar-item">
                        <search/>
                    </div>
                    <div class="navbar-item">
                        <div class="buttons">
                            <a href="/admin/user/1/dashboard" class="button is-primary">
                                <span class="icon-text">
                                  <span class="icon">
                                    <i class="fas fa-home"></i>
                                  </span>
                                  <span><strong>Личный кабинет</strong></span>
                                </span>
                            </a>
                            <a class="button is-light">
                                             <span class="icon-text">
                                  <span class="icon">
                                   <i class="fa-solid fa-cart-shopping"></i>
                                  </span>
                                  <span><strong>Корзина</strong></span>
                                </span>
                            </a>
                        </div>
                    </div>
                </div>
            @endif
        </div>
    </div>

</nav>
