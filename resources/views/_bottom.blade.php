</div>
<footer class="footer has-background-card has-text-white">
    <div class="content has-text-centered">
        <p>
            <strong class="has-text-warning">Diol CMS WEB UI</strong>. The source code is licensed
            <a href="http://opensource.org/licenses/mit-license.php">MIT</a>. The website content
            is licensed <a href="http://creativecommons.org/licenses/by-nc-sa/4.0/deed.ru">CC BY NC SA 4.0</a>.
        </p>
    </div>
</footer>
</body>
    <script src="{{ mix('/js/app.js') }}"></script>
</html>
