<div class="tile is-ancestor">
    <div class="tile is-parent is-3">
        <nav class="vertical-menu">
            <vertical-menu/>
        </nav>
    </div>
    <div class="tile is-vertical is-9">

        <div class="tile">
            <div class="tile is-parent is-vertical">
                <article href="#" class="tile is-child">
                    <picture>
                        <img width="480" height="306" src="https://picsum.photos/id/22/480/306" alt="Image">
                    </picture>
                </article>
            </div>
            <div class="tile is-parent">
                <article href="#" class="tile is-child">
                    <picture>
                        <img width="480" height="306" src="https://picsum.photos/id/23/480/306" alt="Image">
                    </picture>
                </article>
            </div>
        </div>
        <div class="tile is-parent">
            <article class="tile is-child">
                <popular/>
                <picture>
                    <img src="https://picsum.photos/id/24/888/300" alt="Image">
                </picture>
            </article>
        </div>
    </div>

</div>
