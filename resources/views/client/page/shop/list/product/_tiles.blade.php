<div class="tile is-ancestor">
    <div class="tile is-vertical is-parent is-5">
        <div class="tile is-child">
            <slider/>
        </div>
    </div>
    <div class="tile is-vertical">
        <div class="tile">
            <div class="tile is-parent is-vertical is-6">
                <article class="tile is-child notification is-dark">
                    <product-actions buy-text="Купить" :has-one-click="true"/>
                </article>
                <article class="tile is-child notification is-primary">
                    <p class="title">Описание:</p>
                    <div class="content">
                        <p class="subtitle">Poached, scrambled or fried on ciabatta, sourdough or Turkish served with bacon and house made relish.</p>
                    </div>
                </article>
            </div>
            <div class="tile is-parent">
                <article class="tile is-child notification is-warning">
                    <div class="title">
                        Цена: 14
                        <span class="icon">
                                <i class="mdi mdi-currency-usd mdi-32px "></i>
                            </span>
                    </div>
                    <p class="subtitle">Состав:</p>
                    <div class="content">
                        <ul>
                            <li>
                                Бекон
                            </li>
                            <li>
                                Яйца
                            </li>
                        </ul>
                    </div>
                    <p class="title">Дополнительно:</p>
                    <div class="content">
                        <product-params/>
                    </div>
                </article>
            </div>
        </div>
    </div>
</div>
