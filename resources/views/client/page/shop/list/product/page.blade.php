@include('_head')
<section class="has-background-primary mb-5">
    <div class="container">
        <div class="hero is-small">
            <div class="hero-body pl-0">
                <p class="title has-text-white">
                    Bacon and Eggs
                </p>
            </div>
        </div>
    </div>
</section>
<section class="section-categories has-background-white pt-5 mb-5 pb-5">
    <div class="container">
        @include('client.page.shop.list.product._tiles')
    </div>
</section>
<div class="section-reviews-title has-background-primary-light">
    <div class="container">
        <div class="hero is-small">
            <div class="hero-body pl-0">
                <div class="title">
                    Отзывы
                </div>
                <div class="subtitle">
                    Оставьте ваши впечатления и получите бонус
                    <strong class="tag is-danger is-size-6 ml-2">
                        <span class="icon mr-1">
                            <span class="mdi mdi-gift mdi-32px "></span>
                        </span>
                        MAID FUN
                    </strong>
                    на скидку заказа 20%
                </div>
            </div>
        </div>
    </div>
</div>
<section class="section-review-body has-background-light">
    <div class="container py-5">
        <review/>
    </div>
</section>
@include('_bottom')
