@include('_head')
<section class="has-background-primary">
    <div class="container">
        <div class="hero is-small">
            <div class="hero-body pl-0">
                <p class="title has-text-white">
                    Каталог
                </p>
            </div>
        </div>
    </div>
</section>
<section class="section-categories has-background-white pt-5 mb-5">
    <div class="container">
        @include('client.page.shop.list._tiles')
    </div>
</section>
@include('_bottom')
