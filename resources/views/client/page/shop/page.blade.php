@include('_head')
<section class="banner ">
    <div class="parallax-wrap is-relative">
        <parallax-bg/>
    </div>
    <div class="container">
        <main class="hero-banner columns">
            <div class="column">

            </div>
            <div class="column has-background-banner banner-offset px-5 is-5 z-minus">
                <div class="is-flex is-flex-direction-column">
                    <h1 class="title">
                        Choco <span class="has-text-primary">Moto</span> <br>
                        <span class="has-text-primary">Maid</span> Coffee
                    </h1>
                    <div class="subtitle has-text-primary-light">
                        Make coffee with LO
                        <svg xmlns="http://www.w3.org/2000/svg" width="40" height="40" fill="#ae294d" class="is-inline" viewBox="0 0 512 512"><!--! Font Awesome Pro 6.1.1 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license (Commercial License) Copyright 2022 Fonticons, Inc. --><path d="M0 190.9V185.1C0 115.2 50.52 55.58 119.4 44.1C164.1 36.51 211.4 51.37 244 84.02L256 96L267.1 84.02C300.6 51.37 347 36.51 392.6 44.1C461.5 55.58 512 115.2 512 185.1V190.9C512 232.4 494.8 272.1 464.4 300.4L283.7 469.1C276.2 476.1 266.3 480 256 480C245.7 480 235.8 476.1 228.3 469.1L47.59 300.4C17.23 272.1 .0003 232.4 .0003 190.9L0 190.9z"/></svg>
                        E
                    </div>
                </div>
            </div>

        </main>
    </div>
</section>
<section class="has-background-primary">
    <div class="container">
        <div class="hero">
            <div class="hero-body pl-0">
                <p class="title has-text-white pb-3">
                    Добро пожаловать
                </p>
                <p class="subtitle has-text-white is-link">
                    Это стартовый шаблон магазина. <br>
                    Для настройки зайдите в <a href="#">личный кабинет</a>
                </p>
            </div>
        </div>
    </div>
</section>
<section class="section-categories has-background-white pt-5 mb-5">
    <div class="container">
        @include('client.page.shop._tiles')
    </div>
</section>
@include('_bottom')
